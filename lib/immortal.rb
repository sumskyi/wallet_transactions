# frozen_string_literal: true

module Immortal
  def destroy
    disable!
    false
  end

  def delete
    disable!
    false
  end

  private

  def disable!
    return unless respond_to?(:active)
  end

end
