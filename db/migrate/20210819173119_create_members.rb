class CreateMembers < ActiveRecord::Migration[6.1]
  def change
    create_table :members do |t|
      t.string :name, null: false
      t.string :type, null: false
      t.boolean :active, null: false, default: true

      t.timestamps
    end
    add_index :members, :name, unique: true
    add_index :members, :type
  end
end
