class CreateDeals < ActiveRecord::Migration[6.1]
  def change
    create_table :deals do |t|
      t.references :sender, foreign_key: { to_table: :purses, primary_key: :member_id }
      t.references :receiver, foreign_key: { to_table: :purses, primary_key: :member_id }
      t.decimal :amount, precision: 11, scale: 2, null: false

      t.timestamps
    end
  end
end
