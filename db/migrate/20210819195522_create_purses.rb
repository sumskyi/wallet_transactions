class CreatePurses < ActiveRecord::Migration[6.1]
  def change
    create_table :purses do |t|
      t.references :member, foreign_key: true
      t.decimal :balance, precision: 11, scale: 2

      t.timestamps
    end
  end
end
