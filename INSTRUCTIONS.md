# Ruby on Rails developer task
## _Onix_
[![N|Solid](./logo.png)](https://onix-systems.com/)

### Notes
* STI used
* I named the "Wallet" as its synonym "Purse" since application itself named as "Wallet", and this module clashing with a class :/
* "Transaction" is named as "Deal" to not be confused with ActiveRecord transactions ://
* All of SQL transactions are implicit, no need to wrap access to DB to explicit ActiveRecord::Base.transaction{} block
* Users cannot be deleted since there are Foreign Keys between entities. Just deactivated. Deals and Purses cannot be deleted as well.

### Docker
It's very easy to install and deploy in a Docker container.

By default, the Docker will expose port 3000, so change this within the
Dockerfile if necessary. When ready, simply use the Dockerfile to
build the image.

```sh
1. docker-compose run --no-deps web rails new . --force --database=mysql
2. docker-compose run web bundle install
3. chown -R $USER:$USER .
4. git checkout HEAD README.md
5. docker-compose build
6. docker-compose up
7. rm -f db/schema.rb
8. mysql -uroot -h 127.0.0.1 -p
9. grant all privileges on *.* to 'vlad'@'%';
10. docker-compose exec web rake db:drop
11. docker-compose exec web rake db:create
12. docker-compose exec web rake db:migrate
13. chown $USER:$USER db/schema.rb
14. docker-compose exec web rake db:seed
```

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:3000
```
