FactoryBot.define do
  factory :deal do
    sender factory: :purse
    receiver factory: :purse
    amount { 5.00 }
  end
end
