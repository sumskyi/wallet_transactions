FactoryBot.define do
  trait(:empty) do
    balance { 0.00 }
  end

  factory :purse do
    member factory: :user
    balance { 9.99 }
  end
end
