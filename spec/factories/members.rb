# frozen_string_literal: true

FactoryBot.define do
  trait :active do
    active { true }
  end

  trait :disabled do
    active { false }
  end

  factory :member do
    sequence(:name) { |n| "Member #{n}" }
  end

  factory(:user, parent: :member, class: User)
  factory(:team, parent: :member, class: Team)
  factory(:stock, parent: :member, class: Stock)
end
