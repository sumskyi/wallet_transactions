# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Deal, type: :model do
  let(:sender) { create(:user) }
  let(:receiver) { create(:team) }
  let(:sender_purse) { sender.purse }
  let(:receiver_purse) { receiver.purse }
  let(:balance) { 10.00 }
  let(:amount) { 4.00 }


  let(:deal) do
    create(:deal,
           sender: sender_purse,
           receiver: receiver_purse,
           amount: amount
          )
  end

  before do
    sender_purse.update_column(:balance, balance)
  end

  describe '#sender_balance' do
    let(:updated_balance) { 6.00 }

    it 'delegates to sender.purse.balance' do
      expect( deal.sender_balance ).to eql(updated_balance)
    end
  end

  context 'sender doesn\'t have such amount' do
    let(:balance) { 250.00 }
    let(:amount) { 251.00 }

    it 'raises error' do
      expect { deal }.to raise_error(::Deal::NotEnoughAmount)
    end
  end

  context 'amount can\'t be <= 0.0' do
    let(:amount) { 0 }

    it 'raises error' do
      expect { deal }.to raise_error(::Deal::NegativeAmount)
    end
  end

  context 'amount can\'t be > 5_000.00 because of tax service tracking' do
    let(:balance) { 50_000_000.00 }
    let(:amount) { 5_000.05 }

    it 'raises error' do
      expect { deal }.to raise_error(::Deal::ExceededAmount)
    end
  end

  context 'sender is disabled' do
    let(:sender) { create(:user, active: false) }

    it 'raises error' do
      expect { deal }.to raise_error(::Deal::DisabledMember)
    end
  end

  context 'receiver is disabled' do
    let(:receiver) { create(:team, active: false) }

    it 'raises error' do
      expect { deal }.to raise_error(::Deal::DisabledMember)
    end
  end

  context 'happy way' do
    it 'has right amount' do
      expect( deal.amount ).to eql(amount)
    end

    it 'creates a deal' do
      expect { deal }.to change { Deal.count }.by(1)
    end

    it 'updates sender purse total balance on a deal' do
      # 10 - 4
      expect(deal.sender_balance).to eql(6.00)
    end

    it 'updates receiver purse total balance on a deal' do
      # 0 + 4
      expect(deal.receiver_balance).to eql(4.00)
    end
  end
end
