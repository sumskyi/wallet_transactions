require 'rails_helper'

RSpec.describe Purse, type: :model do
  let!(:member) { create(:stock) }
  let(:purse) { Purse.take }
  let(:balance) { purse.balance }

  it 'creates a purse with ZERO balance' do
    expect(balance).to eql(0.00)
  end
end
