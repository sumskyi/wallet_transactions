# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Member, type: :model do
  describe 'creating wallet on member creation' do
    let(:member) { create(:user) }

    it 'creates a wallet for a new member' do
      expect { member }.to change { Purse.count }.by(1)
    end

    it 'new member has a balance 0.0' do
      expect(member.balance).to eql(0.00)
    end
  end
end
