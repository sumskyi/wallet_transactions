# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Member, type: :model do
  describe 'cannot be deleted' do
    let!(:sender) { create(:user) }

    it 'does not deletes' do
      expect { sender.destroy }.to_not change{ Member.count }
    end

    it 'makes a member disabled' do
      sender.delete
      expect(sender.reload.disabled?).to be_truthy
    end
  end
end
