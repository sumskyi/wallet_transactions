# syntax=docker/dockerfile:1
FROM ruby:2.7.4
RUN apt-get update -qq && apt-get install -y default-mysql-client

RUN apt-get install -y apt-utils
RUN apt-get install -y curl

RUN curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
RUN chmod a+x ./nodesource_setup.sh
RUN ./nodesource_setup.sh
RUN apt-get install -y nodejs

WORKDIR /wallet
COPY Gemfile /wallet/Gemfile
COPY Gemfile.lock /wallet/Gemfile.lock
RUN bundle install

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]
