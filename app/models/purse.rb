# frozen_string_literal: true

require 'immortal'

class Purse < ApplicationRecord
  include Immortal

  belongs_to :member

  has_many :incomes, foreign_key: :receiver_id, class_name: 'Deal'
  has_many :costs, foreign_key: :sender_id, class_name: 'Deal'
end
