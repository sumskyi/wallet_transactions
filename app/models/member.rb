# frozen_string_literal: true

require 'immortal'

class Member < ApplicationRecord
  include Immortal
  has_one :purse

  after_save do |new_member|
    Purse.create!(member: new_member, balance: 0.00)
  end

  delegate :balance, to: :purse

  def disabled?
    !active?
  end

  def disable!
    update_column(:active, false)
  end
end
