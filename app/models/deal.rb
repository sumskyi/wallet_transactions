# frozen_string_literal: true

require 'immortal'

class Deal < ApplicationRecord
  include Immortal

  ZERO_AMOUNT = 0
  EXCEEDED_AMOUNT = 5000

  NotEnoughAmount = Class.new(StandardError)
  NegativeAmount  = Class.new(StandardError)
  ExceededAmount  = Class.new(StandardError)
  DisabledMember  = Class.new(StandardError)

  belongs_to :sender,
    foreign_key: :sender_id,
    primary_key: :member_id,
    class_name: 'Purse'

  belongs_to :receiver,
    foreign_key: :receiver_id,
    primary_key: :member_id,
    class_name: 'Purse'

  delegate :balance, to: :sender, prefix: true
  delegate :balance, to: :receiver, prefix: true

  after_save :update_purse

  # too coupled?
  # move to service class?
  class DealValidator < ActiveModel::Validator
    def validate(deal)
      raise NotEnoughAmount if deal.sender.balance <= deal.amount
      raise NegativeAmount if deal.amount <= ZERO_AMOUNT
      raise ExceededAmount if deal.amount >= EXCEEDED_AMOUNT

      if member_disabled?(deal.receiver) || member_disabled?(deal.sender)
        raise DisabledMember
      end
    end

    private

    def member_disabled?(purse)
      purse.member.disabled?
    end

  end
  validates_with DealValidator

  private

  # pleaase note there is non-explicit SQL transaction
  def update_purse
    sender.update_column(:balance, sender_balance - amount)
    receiver.update_column(:balance, receiver_balance + amount)
  end
end
